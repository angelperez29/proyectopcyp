/*
Clase de los recurso compartido
*/
public class Data {
    int data [][];
    int lucky=0;
    int validation=0;
    int validationAdmin=0;
    //Metodo constructor
    public Data(){
        data = new int [10][2];
        for(int i=0;i<10;i++){
            for(int j=0;j<2;j++){
                data[i][j]=0;
            }
        }
    }
    /*
    Metodo: setData
    Función: Ingresa los datos de los jugadores
    Argumentos:
        id: Idenfificador del proceso y posición en donde se guardaran los datos
        number: Número elegido por el proceso jugador 
        status: 1 si el jugador adivino el número, 0 en caso contrario
    */
    public synchronized void setData(int id, int number, int status){
        data[id][0]=number;
        data[id][1]=status;
    }
    /*
    Metodo: notSeaved
    Función: Verificar si en la posición ya se encuentra almacenado algo
    Argumentos:
        id: Identificador del proceso y posición del dato
    */
    public synchronized boolean notSeaved(int id){
        if(data[id][0]==0){
            return true;
        }
        return false;
    }
    /*
    Metodo:setLucky
    Función: Almacenar el número provilegiado
    Argumentos:
        lucky: valor para el número privilegiado
    */
    public synchronized void setLucky(int lucky){
        this.lucky=lucky;
        notifyAll();
    }
    /*
    Metodo: turnDraw
    Función: determinar el si ya es hora de que el proceso sorteo comienze su ejecuión
    Argumentos:

    */
    public synchronized void waitTurn(){
        int count = 0; 
        for(int i=0;i<10;i++) if (data[i][0]==0) count ++;
        try {
            if(count!=0) wait();
        } catch (Exception e) {}
    }
    /*
    Metodo: turnScrutiny
    Función: determinar el si ya es hora de que el proceso Escrutino comienze su ejecuión
    Sin Argumentos
    */
    public synchronized void turnScrutiny(){
        int count = 0; 
        for(int i=0;i<10;i++) if (data[i][0]==0 || lucky == 0) count ++;
        try {
            if(count!=0) wait();
        } catch (Exception e) {}
    }
    /* 
    Metodo: waitTurnWrite
    Función dormir al proceso admin hasta que todos los datos esten listos para ser escritos en el archivo
    Sin argumentos
    */
    public synchronized void waitTurnWrite(){
        int count = 0; 
        for(int i=0;i<10;i++) if (data[i][0] == 0 || lucky == 0 || validation == 0)
                count++;
        try {
            if(count!=0) wait();
        } catch (Exception e) {}
    }
    /* 
    Metodo: waitTurnWinA
    Función: dormir al proceso jugador hasta que todos los datos esten listos para ser vistos por
             los jugadores 
    Sin argumentos
    */
    public synchronized void waitTurnWinA(){
        int count = 0; 
        for(int i=0;i<10;i++) if (data[i][0] == 0 || lucky == 0 || validationAdmin == 0)count++;
        try {
            if(count!=0) wait();
        } catch (Exception e) {}
    }
    /*
    Metodo:changeTurn
    Función: notificar a todos los procesos que pueden realizar su trabajo
    Sin argumentos
    */
    public synchronized void changeTurn(){
        notifyAll();
    }
    /*
    Metodo:changeTurnScrutiny
    Función: notificar a todos los procesos que pueden realizar su trabajo
    Sin argumentos
    */
    public synchronized void changeTurnScrutiny(){
        validation=1;
        notifyAll();
    }
    /*
    Metodo:changeTurnAdmin
    Función: notificar a todos los procesos que pueden realizar su trabajo
    Sin argumentos
    */
    public synchronized void changeTurnAdmin(){
        validationAdmin=1;
        notifyAll();
    }
    /*
    Metodo:getData
    Función: devolver la tabla donde se va almacenando la información 
    Sin argumentos
    */
    public synchronized int [][] getData(){
        return data;
    }
    /*
    Metodo:getData
    Función: devolver el número privilegiado
    Sin argumentos
    */
    public synchronized int getNumber(){
        return lucky;
    }

}