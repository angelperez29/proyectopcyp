public class Sorteo extends Thread implements InterfazUI{
    Data data;
    
    /*
    Metodo constructor
    Argumentos: 
        data: objeto de la clase de recurso compartido
        admin: objeto de la clase administradora
    */
    public Sorteo(Data data){
        this.data=data;
    }

    /*
    Metodo de la clase interfaz
    */
    public void outputCommandLine(String msgs[]){
        for (String msg : msgs) {
            System.out.println("\t\t"+greenBG+black+msg+rest);
        }
    }

    /*
    Metodo que se ejecuta al lanzarse el proceso
    */
    public void run(){
        data.waitTurn();
        int num=(int) (Math.random()*20)+1;
        outputCommandLine(new String[]{"El número provilegiado es: "+num});
        data.setLucky(num);
    }
}