public class Escrutino extends Thread implements InterfazUI{
   Data data;
   /*
   Metodo constructor
   */
   public Escrutino(Data data){
      this.data=data;
   }

   //Metodo de la clase interfaz
   public void outputCommandLine(String msgs[]){
      for (String msg : msgs) {
         System.out.println("\t\t"+blackBG+white+msg+rest);
      }
   }
   /*
   Metodo que se realiza cuando se lanza el proceso escrutino
   */
   public void run(){
      data.turnScrutiny();
      int table[][]=data.getData();
      int lucky=data.getNumber();
      outputCommandLine(new String []{"Escrutino dice:","\t Iniciando la comprobación la comprobación...."});
      for(int i=0;i<10;i++){
         if(table[i][0]==lucky)data.setData(i, table[i][0], 1);
      }
      outputCommandLine(new String []{"Escrutino dice:","\t He terminado con la comprobación"});
      data.changeTurnScrutiny();
   }
}