public class Jugadores extends Thread implements InterfazUI{
    int mynum;
    Admin admin;
    Data data;
    int id;
    /* 
    Metodo constructor
    Parametros:
        id: identificador del jugador
        admin: clase del proceso administrador
    */
    public Jugadores(int id, Admin admin,Data data){
        setName("Soy el jugador "+id);
        this.id=id;
        this.admin=admin;
        this.data=data;
    }
    //Metodo de la clase InterfazUI
    public void outputCommandLine(String msgs[]){
        for (String msg : msgs) {
            System.out.println(msg+rest);
        }
    }
    /*
    Metodo: winA
    Función: el proceso avisa si ha adivinado o no 
    */
    public void winA(){
        int table[][]=data.getData();
        if(table[id-1][1]==1){
            outputCommandLine(new String[]{"\t\t"+blueBG+black+getName()+" y se me informa que he adivinado "});
        }else{
            outputCommandLine(new String[]{"\t\t"+blueBG+black+getName()+" y se me informa que no he adivinado "});
        }
    }
    /*
    Metodo que se ejecuta al lanzarce a ejecución cualquier proceso
    */
    public void run(){
        mynum = (int) (Math.random()*20)+1;
        admin.setDataPlayers(id, mynum);
        outputCommandLine(new String[]{"\t\t"+whiteBG+black+getName()+" y mi número es "+mynum});
        data.waitTurnWinA();
        winA();
        data.changeTurn();
    }
}