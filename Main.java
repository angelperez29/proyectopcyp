
public class Main {
    Jugadores jugador[]=new Jugadores[10]; //Hilos de jugadores
    Admin admin;
    Data data;
    Escrutino escrutino;
    Sorteo sorteo;

    /*
    Método constructor de la clase principal
    Argumento:
        repetitions: número de veces que se quiere jugar
    */
    public Main(int repetitions){
        do{
            System.out.println("-----------------------------------------------------------------------------------");
            runProcess();
        }while((repetitions--)>1);
    }
    
    /*
    Metodo: runProcess
    Función: Encargado de lanzar lo procesos
    */
    public void runProcess(){
        data = new Data();
        sorteo= new Sorteo(data);
        escrutino=new Escrutino(data);
        admin=new Admin(data);
        for(int i=0;i<10;i++){
            jugador[i]=new Jugadores(i+1,admin,data);
            jugador[i].start();
        }
        admin.start();
        sorteo.start();
        escrutino.start();
        for (Jugadores player : jugador){
            try {
                player.join();
            } catch (InterruptedException e) {} 
        }
        try {
            admin.join();
            sorteo.join();
            escrutino.join();
        } catch (InterruptedException e) {
        }
    }

    public static void main(String[] args) {
        int repetitions=0;
        if(args.length!=0) repetitions = Integer.parseInt(args[0]);
        new Main(repetitions);
    }
}