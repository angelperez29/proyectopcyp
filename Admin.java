//Clases para archivos
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Date;
/*
Clase del proceso Administrador
*/
public class Admin extends Thread implements InterfazUI{
    Data data;
    int lucky;

    /*
    Metodo constructor
    Parametros: 
        data: clase de el recurso compartido
    */
    public Admin(Data data){
        this.data=data;
        lucky=0;
        setName("Hola soy admin");
    }

    /*
    Metodo abtracto de la clase InterfazUI para impresión de mensajes
    */
    public void outputCommandLine(String msgs[]){
        for (String msg : msgs) {
            System.out.println(msg+rest);
        }
    }
    /*
    Metodo: setDataPlayers
    Función: almacenar los numeros de los procesos jugadores
    Argumentos:
        id: identificador del proceso
        number: numero elegido
    */
    public void setDataPlayers(int id, int number){
        if(data.notSeaved(id-1)){
            data.setData(id-1, number, 0);
            outputCommandLine(new String []{"\t"+yellowBG+black+getName()+" y voy a guardar tu numero jugador "+id+"  "});
        }
        outputCommandLine(new String []{"\t"+yellowBG+black+getName()+" te informo jagador "+id+" que tu número ya estaba guardado  "});
    }
    /*
    Metodo: writeData()
    Función: almacenar el estatus de cada jugador en el archivo txt y avisar a los procesos su situación
    Argumentos:
        id: identificador del proceso
        number: numero elegido
    */
    public void writeData(){
        int table[][]=data.getData();
        int lucky=data.getNumber();
        try (PrintWriter file = new PrintWriter(new FileWriter("resultBingo.txt", true))){
            Date fecha = new Date();
            String str="-----------------------"+fecha.toString()+"---------------------------";
            file.println(str);
            str="-----------------------> El número provilegiado es:"+lucky+"<------------------------";
            file.println(str);
            for(int i=0; i<10;i++){
                if(table[i][1]==1){
                    str ="Jugador "+(i+1)+" con número "+table[i][0]+" ha acertado";
                }else{
                    str ="Jugador "+(i+1)+" con número "+table[i][0]+" no ha acertado";
                }
                file.println(str);
            }
            file.close();
        } catch (Exception e) {}
    }
    
    /*
    Metodo de ejecucioń del proceso Administrador 
    */
    public void run(){
        data.waitTurnWrite();
        outputCommandLine(new String[]{"\t"+cyanBG+black+getName()+" he recibido los resultados se los informo"});
        writeData();
        data.changeTurnAdmin();
    }
}