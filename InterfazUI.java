public interface InterfazUI {
    //Colores que puede tomar el texto del mensaje
    public static final String black = "\u001B[30m";
    public static final String white = "\u001B[37m";
    public static final String yellow = "\u001B[33m";
    public static final String rest = "\u001B[0m";//Restablece los valores por defecto de consola
    //Colores de fondo para los mensajes
    public static final String blackBG = "\u001B[40m";//Color de fondo para Escrutino
    public static final String greenBG = "\u001B[42m";//Color de fondo para Sorteo
    public static final String yellowBG = "\u001B[43m";//Color de fondo para Admin
    public static final String whiteBG = "\u001B[47m";//Color de fondo para Jugadores
    public static final String cyanBG = "\u001B[46m";
    public static final String blueBG = "\u001B[44m";



    /*  
    Metodo: outputCommandLine
    Función: Mostrar mensajes al usuario
    Argumento: msgs[], es un arreglo con los mensajes que se desean mostrar al usuario 
    */
    public void outputCommandLine(String msgs[]);
}